#!/bin/sh

branch=$1
git checkout $branch
git fetch
git reset --hard origin/$branch
chmod 755 -R /home/www/service/web-conf-ghp

exit 0

