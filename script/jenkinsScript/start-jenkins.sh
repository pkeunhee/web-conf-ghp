#!/bin/bash
JENKINS_WAR=/usr/local/jenkins.war
JENKINS_LOG=./jenkins.log
JAVA=java
JENKINS_PORT=8200
nohup nice $JAVA -jar $JENKINS_WAR --httpPort=$JENKINS_PORT > $JENKINS_LOG 2>&1 &
