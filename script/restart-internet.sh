#!/bin/sh
#This makes sure it is connected to 128.*** address
connect=`/sbin/ifconfig -a | sed -n '2p' | awk '{print $2}' | sed 's/addr://' | sed 's/[.].*//'`
if [ "$connect" != "192" ]; then
/etc/init.d/network restart
fi